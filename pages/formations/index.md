---
title: Organisme de formation
eleventyNavigation:
  key: Organisme de formation
  parent: Accueil
  order: 3
---

# Organisme de formation

Solstice SCOP est un organisme de formation certifié Référentiel National Qualité (Qualiopi) au titre des actions de formation depuis le 1er octobre 2021.
Nous développons et portons des activités de Formation Professionnelle Continue avec une exonération de TVA et un accès aux fonds de financements publics de la formation.

![Solstice est un organisme certifié Qualiopi](img/logo_solstice_qualiopi.png)

### Taux de satisfaction • Chiffres 2022

**En 2022, 42 intervenants•es en formation ont réalisé un total de 29.455 heures-stagiaires.**

- **99%** des stagiaires sont satisfaits ou très satisfaits par leur formation.
- **98%** des stagiaires estiment avoir tout à fait atteint les objectifs de leur formation.
- **98%** des stagiaires sont très satisfaits ou statisfaits de la qualité de l'animation prédagogique.
- **98%** des stagiaires sont statisfaits ou  très statisfaits de l'organisation de leur formation.

## Vous êtes formatrice·teur ?

<div class="row">
<div class="col-12 col-md-4">

Solstice héberge **vos formations**.

[En savoir plus ? Contactez-nous !](/contact/#contact){ .btn .cta }
</div>

<div class="col">

Chaque année, une quarantaine d'entrepreneur·es solsticien·nes hébergent leur activité de formation dans la coopérative. Le **[catalogue](/formations/catalogue/)** proposé couvre une vingtaine de thématiques. La démarche de certification Qualiopi est portée par les entrepreneur·es formatrice·teurs et l'équipe support. l'objectif de cette démarche est de structurer l'exercice de ce métier pour répondre aux évolutions du marché de la Formation professionnelle continue. Tou·te·s les entrepreneur·es formatrice·teurs respectent le cahier des charges de l'OF Solstice. Ils·Elles sont formée·s et accompagné·es pour répondre aux exigences règlementaires et s'engagent dans une démarche commune d'amélioration continue.

Chaque **[formateur·trice](/formations/catalogue/)** est en charge de son portefeuille client·es, communique auprès de ses client·es (web, mail, plaquette…) et peut proposer des formations au **[catalogue](/formations/catalogue/)** ou sur mesure (à la demande, en fonction des besoins des clients), en présentiel et/ou à distance. Certains contenus et tarifs peuvent être personnalisables (notamment pour les Solsticien·nes).

</div>
</div>

## Vous cherchez une formation ?


<div class="row">
<div class="col-12 col-md-4">

Nos formateur·ices proposent **de nombreuses formations**.

[Consulter le catalogue](/formations/catalogue/){ .btn .cta }

</div>

<div class="col">

Quelques-unes des thématiques proposées :

- Accompagnement, relation d’aide, médiation
- Massages bien-être
- Communication inter-personnelle
- Compostage et gestion des déchets
- Data sciences, Intelligence artificielle
- Écriture, Communication écrite
- Gestion de projets participatifs, Intelligence collective
- Intervention en institutions, Pédagogie
- Langues, Lecture labiale, Langue des signes
- Management, Communication, RH
- Ostéopathie animale
- Psychologie, Psychothérapie, Psycho corporelle
- Techniques textiles
- Web, RGPD, Digital, Bureautique

</div>
</div>

## Accessibilité aux personnes en situation de handicap
Pour toute information concernant nos conditions d'accès au public en situation de handicap (locaux, adapatation des moyens de la prestation), merci de contacter notre référent handicap : Bertrand Barrot (bertrand.barrot(@)solstice.coop).

## Vous avez besoin d’être formé·e au métier de formateur·trice ?

<div class="row">
<div class="col-12 col-md-4">

Solstice vous propose une **formation en trois modules**.

[Vous souhaitez recevoir le programme ? Contactez-nous](/contact/#formation-formateurice){ .btn .cta }
</div>

<div class="col">

Que votre activité de formation soit hébergée ou non chez Solstice, cette formation est pour vous si vous êtes :
- formateur·trice occasionnel·le
- formatrice·teur confirmé·e
- nouvelle et nouveau formatrice·teur

**Pourquoi ?**
- Acquérir ou réviser les outils de base
- Revoir sa pratique sous un autre angle
- Mettre à jour ses connaissances sur la formation pro
- Animer efficacement une formation
- Structurer professionnellement son offre de formation (ingénierie)

</div>
</div>

![Solstice est un organisme certifié Qualiopi](img/logo_solstice_qualiopi.png)
