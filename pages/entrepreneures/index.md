---
title: Annuaire d'entrepreneur·es de la vallée de la Drôme
eleventyNavigation:
  key: Les entrepreneur·es
  parent: Accueil
  order: 4
scripts:
  - /js/controllers/masonry.js
---

:::info Annuaire en cours de réalisation
L'annuaire détaillé est en train d'être alimenté par la centaine d'entrepreneur·es de la coopérative.
:::

<div id="entries" class="row entrepreneures">
{% for person in people %}
  <article class="col-sm-6 col-lg-4 mb-4" data-id="{{ person.id }}" data-order="{{ loop.index0 }}">
    <div class="card position-relative shadow-hover">
      {%- set name = [person.first_name, person.last_name] | join(' ') | capitalize -%}
      {%- set src = person.photo_link or person.logo_link -%}
      {%- if src -%}
        {% image src, name, { class: "card-img-top h-auto" } %}
      {%- endif -%}
      <div class="card-body">
        <h2 class="card-title text-center text-capitalize">
          <a href="/entrepreneures/{{ name | slugify }}/" class="stretched-link">
            {{ name }}
          </a>
        </h2>
        <div class="card-text">{{ person.title }}</div>
        {%- if person.tag_names | length -%}
        <ul class="list-inline mt-2 mx-n2">
          {% for tag in person.tag_names -%}
            <li class="list-inline-item"><span class="badge badge-pill text-bg-light mb-1">{{ tag }}</span></li>
          {%- endfor %}
        </ul>
        {%- endif -%}
      </div>
    </div>
  </article>
{% endfor %}
</div>
