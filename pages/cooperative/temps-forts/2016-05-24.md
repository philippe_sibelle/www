---
title: "Séminaire sur la question de la gouvernance en SCOP"
subtitle: 24 mai 2016
---

Solstice évolue dans un environnement complexe et en perpétuel mouvement. Pour poursuivre son développement et conserver une place particulière sur son territoire, les associés de Solstice sont régulièrement amenés à initier de nouveaux projets ou actions.

Hervé Charmettant, membre de l’équipe [Projet SCOP,](https://projetscop.blogspot.com/) nous a présenté la première partie de son étude publiée en octobre 2015,
**Les SCOP : Quels modèles d’entreprises ? Des entreprises modèles ?**

L’équipe « Projet Scop » rassemble six universitaires rattachés à l’Université Grenoble-Alpes, de formation économique, gestionnaire et sociologique.

Le Projet Scop est né de la curiosité pour ces entreprises particulières qui, malgré l’attention qui leur est portée aujourd’hui, sont encore mal connues.

Les études de terrain ont porté sur les relations sociales en s’intéressant à tout ce qui a trait au fonctionnement interne de l’entreprise, à la transformation d’entreprises classiques en Scop, décryptant les freins et les moteurs de ce processus, et sur l’apport de ces entreprises « pas comme les autres » au dynamisme territorial, ainsi qu’aux liens entre la coopération en interne et en externe qu’elles déploient.

[Restitution](/files/Schéma_Restitution_séminaire_HC_02.pdf)

Ce schéma ne prétend pas rendre compte de l’ensemble des échanges in extenso. L’idée est de retranscrire les propos tenus et échangés, dans leur jus, pris en notes, tels qu’ils l’ont été ce jour, non rédigés, en utilisant des mots-clés parfois, et sans analyse a posteriori.

![Séminaire mai 2016](img/temps-forts/2016-05-24/Seminaire-Solstice-mai2016.jpg)
