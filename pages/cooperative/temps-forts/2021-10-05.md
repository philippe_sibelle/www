---
title: Rencontre coopérative d'automne
subtitle: 5 octobre 2021
carousel:
  - img/temps-forts/2021-10-05/20211005_142337-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_144816-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_144938-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101044-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101040-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101122-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101139-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101507-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_125207-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_142307-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_142319-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_100941-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_100955-scaled.jpg
  - img/temps-forts/2021-10-05/20211005_101009-scaled.jpg
---

Faire connaissance, nous voir et nous revoir, nous régaler d’un buffet partagé de très bonne tenue, **la rencontre d’automne fut aussi l’occasion** de travailler ensemble en ateliers animés par :

- **Sarah Loyer : Formations, formatrices, formateurs, Qualiopi et tout ce qui s’en suit !**

    Où l’on réalisa que cette plénière aurait pu s’étendre toute la journée tellement on y était bien !
    Apport d’infos, échanges et interconnaissance, une rencontre cousue main pour célébrer notre certification Qualiopi et tout ce qui s’en suivra !

- **Marie-Dominique Texier : S’ouvrir à la confiance dans sa vie d’entrepreneur·e**

    Où l’on se retrouva à 40, assis, yeux fermés, dans la pénombre de la salle feutrée, à visualiser nos clients de rêve, nos clients dans l’idéal, ceux avec lesquels on a envie de travailler, de conclure un contrat et d’avoir une vraie relation de travail.

- **Jean-Jacques Magnan : Financer son activité**

    Où l’on a envisagé le fait qu’une activité professionnelle demande de l’investissement si l’on veut se donner les moyens de développer ses services et les faire connaître. Et que c’est l’angle de notre rapport à l’investissement et à l’argent qui peut bouger.

- **Stéphane Pignal : S’approprier les bons outils de la bonne gestion de son activité : le bon pilotage de mon activité**

    Où l’on découvrit que l’on peut être plutôt « Trézo-ascendant-Budget-Prév » en signe astro-comptable, que l’on ne peut pas tricher sur le compte de résultat, que l’export du Compte de résultat sur Louty est tristounet et qu’il est urgent d’y rajouter du jaune ! Et que ça fait partie de sa bonne gestion de prendre ses congés payés (qui ont été provisionnés).

- **Cédric Roussel :** **En tant que Solsticien·ne, combien devrais-je, au minimum, facturer mon heure de travail ?**

    Où l’on a fait le lien entre le besoin d’argent, pour vivre, le chiffre d’affaire que l’on doit générer.
    Et le constat que le rapport à l’argent est vraiment très différent pour chacun.
    Et un fil commun : on n’ose jamais en demander assez. Comment dès lors concilier contraintes de territoires et besoins personnels ?
    Une bonne réflexion sur la subjectivité d’un montant facturé et le lien que l’on peut avoir avec sa propre réalité et son idéal (travailler pour « rien » mais aider une cause importante pour soi, ou une personne en difficulté, aider au lancement d’un projet, coopérer…).
    Bref, une plongée en direct dans la sphère très intime du rapport à l’argent et le lien à son territoire dans la confiance et la bienveillance.

