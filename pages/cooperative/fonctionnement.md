---
title: Fonctionnement de la coopérative Solstice
figma: https://www.figma.com/file/cuBvusShQ85ecBMBIqwnlf/Site?node-id=228%3A420
eleventyNavigation:
  key: Fonctionnement
  parent: Vie coopérative
---

## Travailler autrement
En CAE, votre activité économique vous permet de vous salarier au sein de la coopérative : vous signez un contrat de travail, vous percevez un salaire et vous bénéficiez d'une protection sociale. [Découvrir et intégrer une CAE](https://www.les-cae.coop/system/files/inline-files/Entreprendre%20en%20CAE%20-%20Plaquette%202022%20VDiff.pdf)  
Chez Solstice, nous considérons qu'une activité dans une CAE fonctionne comme une entreprise : elle doit être lucrative, dégager du résultat, être rentable, permettre d'investir. Par ailleurs, selon nos principes coopératifs, la recherche de profit économique reste subordonnée à l'épanouissement des coopératrices et coopérateurs salarié·es. 

### Être accompagné·e individuellement et collectivement ?

- A minima, vous faites le point lors de deux rendez-vous annuels avec votre accompagnant·e référent·e (loi ESS), et bénéficiez d'un suivi individuel régulier, à la demande, en fonction de vos besoins et de vos objectifs.
- Une offre de formations coopératives et d’ateliers qui se déroulent dans les temps de rencontres coopératives (de une heure en visio à une journée en présentielle) constitue le socle de l’accompagnement collectif et couvre des thématiques telles que  la posture entrepreneuriale, le pilotage d’activité à partir de l’outil de gestion, la démarche commerciale, la communication digitale, la réponse aux appels d’offre, le sociétariat et la culture d’entreprise coopérative.
- Supports dédiés par email, chat et permanence téléphonique tous les matins.
- Développement de projets avec d'autres entrepreneur·es.
- Veille et réponses à des appels d’offres collectivement en associant d’autres entrepreneur·es.

### Une équipe de personnes humaines dédiées

Élise, Philippe, Marion, Laurie, Valérie, Stéphane et Bertrand
_Photos & organigramme à insérer_  

### Quels moyens mutualisés, au service de quoi ?
**Chez Solstice, vous disposez de moyens mutualisés et de solutions pour faciliter votre activité au quotidien** :  
- vous êtes entrepreneur·e salarié·e (autonome et en CDI)
- vous êtes accompagné·e et vous vous formez, individuellement et collectivement
- vous bénéficiez de services administratifs, comptables et juridiques réalisés par de vraies personnes qui vous répondent toujours
- vous utilisez des outils de gestion numériques dédiés
- Utilisation du N° de Siret et du N° de TVA de la coopérative pour constituer une trésorerie tout en gardant ses droits chômage pendant la durée du CAPE.
- Facturation avec [LOUTY](https://www.louty.fr/) (gestion des factures et des devis) de vos prestations à vos clients, selon les devis et contrats établis.
- Tenue d’une compta analytique de votre activité  et restitution dans l’outil de gestion : compte de résultat, trésorerie disponible, Grand livre des tiers, Détails des comptes, Balances et Grand Livre Analytique.
- Prise en charge des tâches administratives, sociales, fiscales et comptables liées à votre activité (paiement de la responsabilité civile pro, contributions fiscales et sociales obligatoires, primes d’assurances, et TVA due au titre de votre activité).
- Certification Qualiopi
- Outil de gestion des formations : Formasol génère des contrats, des conventions, feuilles de présences et catalogue de vos formations pro.
- Crédit Impôts Recherche (CIR) : vos clients récupèrent 30% du montant facturé en crédit d’impôt.

## Le statut d'entrepreneur·e-salarié·e
Le parcours (insérer schéma)  
Ce que permet le statut ESA : tous les avantages du salariat (chômage, retraite, formation, mutuelle, prévoyance).  
Vous sécurisez votre parcours et bénéficiez d'une protection sociale.
(à développer).  
