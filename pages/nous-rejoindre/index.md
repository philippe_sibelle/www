---
title: Comment rejoindre la coopérative d'activité Solstice ?
eleventyNavigation:
  key: Nous rejoindre
  parent: Accueil
  order: 90
---

# Nous rejoindre

C’est ici que commence votre projet en trois temps :

1. **Assister à une réunion d’information collective**
Les réunions collectives ont lieu deux fois par mois. Elles ont pour objet de présenter le modèle social, économique et coopératif de la CAE Solstice, le mécanisme de contribution et le parcours entrepreneurial lié au statut d'entrepreneur·e-salarié·e.\
[Participer à la prochaine réunion](/nous-rejoindre/reunion-information/){.btn .cta}

2. **Affiner votre projet avec votre futur·e accompagnante**
[Prendre rendez-vous avec un·e accompagnant·e suite à une réunion d'info](/contact/#accompagnant){.btn .cta}

3. **Démarrer votre contrat**

## Besoin d'échanger de vive voix avant toute chose ?

Nous répondons au téléphone les lundi, mardi, jeudi et vendredi, de 9h à midi : <a href="tel:+33475253230">04 75 25 32 30</a>
