import Masonry from "https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/+esm"

const init = () => {
  new Masonry("#entries", { percentPosition: true })
}

if (document.readyState !== "loading") {
  init()
} else {
  document.addEventListener("DOMContentLoaded", init)
}
