function attachFormEvents (form) {
  form.addEventListener('submit', (event) => {
    event.preventDefault()
    const submitButton = form.querySelector('.btn[type="submit"]')
    const errorMessage = document.querySelector(`#form-${form.id}-error`)

    submitButton.classList.add('disabled', 'submitting')
    errorMessage.classList.add('d-none')

    fetch(form.getAttribute('action'), {
      method: 'POST',
      body: new FormData(form),
      mode: 'cors',
      cache: 'no-cache',
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "Origin": window.location.host
      }
    })
      .then(response => {
        if (response.ok) {
          displayConfirmation(form)
          form.reset()
        }
        else {
          throw Error(response)
        }
      })
      .catch(() => {
        errorMessage.classList.remove('d-none')
      })
      .finally(() => {
        submitButton.classList.remove('disabled', 'submitting')
        form.scrollIntoView({ behavior: 'smooth', block: 'start' })
      })
  })
}

function displayConfirmation(form) {
  let title, option
  let content = `Merci pour votre intérêt !`
  const select = form.querySelector(".form-select")
  if (select) {
    title = `Confirmation d'inscription`
    option = select.selectedOptions[0]
    content += eventConfirmationDetails(option)
  } else {
    title = `Confirmation`
    content += "<br>Nous vous recontactons dès que possible."
  }
  fillAndShowModal(title, content)
}

function eventConfirmationDetails(option) {
  const date = option.value
  const duration = option.dataset.duration // Toute la journée | 1,5h
  const location = option.dataset.location // http… | Solstice | en visio
  const mode = option.dataset.mode // ROUTE | EMPTY | VISIO | ADDRESS
  let content = `<br>Vous êtes inscrit•e à l'évènement du ${date}`
  if (duration) {
    content += ` (durée : ${duration}).`
  }
  if (mode == "VISIO") {
    content += `<br>L'évènement se déroule en visio à l'adresse suivante : <strong class="user-select-all">${location}</strong>.`
  } else if (mode == "ROUTE") {
    content += `<br>Voici un lien vers le lieu de l'évènement : <a href="${location}" target="_blank">${location}</a>.`
  } else if (mode == "ADDRESS") {
    content += `<br>L'évènement se déroule à l'adresse suivante : <strong class="user-select-all">${location}</strong>.`
  }
  return content
}

function preselectEvent (search) {
  const params = new URLSearchParams(search)
  const eventId = params.get('event')
  const option = document.querySelector(`option[data-event="${eventId}"]`)

  if (option) {
    const { index } = option
    option.parentElement.selectedIndex = index
  }
}

function fillAndShowModal(title, content) {
  const $el = document.querySelector("#modal")
  $el.querySelector("#modal-title").innerHTML = title
  $el.querySelector("#modal-body").innerHTML = content
  // Automatically open the modal
  new Bootstrap.Modal($el).show()
}

document.addEventListener('DOMContentLoaded', () => {
  Array.from(document.querySelectorAll('.js-external-form'))
    .forEach(form => attachFormEvents(form))

  preselectEvent(document.location.search)
})
