import MiniSearch from "https://cdn.jsdelivr.net/npm/minisearch@6.0.1/+esm"

const container = document.querySelector('#results')
const data = JSON.parse(document.querySelector('#search-data').innerText)
const form = document.querySelector('#search-form')
const searchField = document.querySelector('#search-field')
const emptyMessage = document.querySelector('#search-empty')
const queryFromURL = new URLSearchParams(document.location.search).get("q")
const template = document.querySelector('#result-template').innerHTML.trim()

const index = new MiniSearch({
  idField: 'url', // Use URL as unique identifier
  fields: ['title', 'content'], // Full-text search these fields
  storeFields: ['url', 'title', 'content'],
  searchOptions: {
    boost: { title: 3 }, // Boost name and tags for better sorting
    prefix: true, // Match word start, allowing 'psy' to match 'psychologue'
    fuzzy: 0.1 // Allow some fuzziness
  }
})
index.addAll(data)

const performSearch = (e) => {
  if (e) e.preventDefault()
  const keywords = searchField.value.trim()
  container.innerHTML = ""
  container.classList.remove("visibility-hidden")

  if (keywords == "") {
    return
  }
  const results = index.search(keywords)
  if (results.length) {
    let matches = []
    results.forEach(result => matches.push(formatResult(result)))
    container.insertAdjacentHTML("beforeend", matches.join("\n"))
  } else {
    emptyMessage.classList.remove("visibility-hidden")
    emptyMessage.focus()
  }
}

const formatResult = (result) => {
  return template.replaceAll("__URL__", result["url"])
                 .replaceAll("__TITLE__", result["title"])
                 .replaceAll("__CONTENT__", result["content"].slice(0, 256) + "…")
}

const init = () => {
  searchField.value = queryFromURL
  performSearch()
}

form.addEventListener('input', () => {
  container.classList.add("visibility-hidden")
  emptyMessage.classList.add("d-none")
})
form.addEventListener('input', performSearch)
form.addEventListener('submit', (e) => e.preventDefault())

if (document.readyState !== 'loading') {
  init()
} else {
  document.addEventListener('DOMContentLoaded', init)
}
