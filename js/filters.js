const { createReadStream } = require('fs')
const { toHours } = require('duration-fns')
const Image = require("@11ty/eleventy-img")
const parse = require('csv-parse')
const production = process.env.ELEVENTY_RUN_MODE == 'build'
const baseUrl = production ? 'https://solstice.coop' : 'http://localhost:8080'

module.exports = function Filters (eleventyConfig) {
  eleventyConfig.addNunjucksFilter("where", where)
  eleventyConfig.addNunjucksFilter("pickattr", pickattr)
  eleventyConfig.addNunjucksFilter("removeattr", removeattr)
  eleventyConfig.addNunjucksFilter("findTrainingProfile", findTrainingProfile)
  eleventyConfig.addNunjucksFilter("findTrainingsFor", findTrainingsFor)
  eleventyConfig.addNunjucksFilter("locationMode", locationMode)
  eleventyConfig.addNunjucksFilter("locationShort", locationShort)
  eleventyConfig.addNunjucksFilter("location", location)
  eleventyConfig.addNunjucksFilter("durationToTime", durationToTime)
  eleventyConfig.addNunjucksFilter("toDateTime", toDateTime)
  eleventyConfig.addNunjucksFilter("toDate", toDate)
  eleventyConfig.addNunjucksFilter("inHours", inHours)
  eleventyConfig.addNunjucksFilter("inDays", inDays)
  eleventyConfig.addNunjucksFilter("antiSpam", antiSpam)
  eleventyConfig.addNunjucksFilter("striptag", striptag)
  eleventyConfig.addNunjucksFilter("filter", filter)
  eleventyConfig.addFilter("absoluteUrl", (path) => baseUrl + path);
}

// Add filter shortcode, because nunjucks.selectattr only works with truthy values
function filter (arr=[], key='', value) {
  return arr?.filter(item => item[key] === value)
}

function antiSpam (email) {
  return munge(email, { encoding: 'utf8' })
}


function totalTime (filePath, columns = ['date', 'person', 'duration']) {
  let total = 0
  let people = {}

  const parser = parse({
    delimiter: ',',
    trim: true,
    skip_empty_lines: true,
    from_line: 2,
    columns,
  })

  return new Promise((resolve) => {
    const stream = createReadStream(filePath).pipe(parser)

    stream.on('error', (error) => console.error(error))

    parser.on('readable', function(){
      let record;
      while ((record = parser.read()) !== null) {
        const [hours, minutes] = record.duration.split(/\D/)
        const duration = (parseFloat(hours || 0) * 60) + parseFloat(minutes || 0)
        const person = String(record.person).toLocaleLowerCase()

        total += duration
        people[person] = (people[person] ?? 0) + duration
      }
    })

    stream.on('end', () => {
      const result = {
        total,
        ...people
      }

      resolve(result)
    })
  })
}


function findTrainingProfile ({ ref }) {
  return this.ctx.people.find(({ id }) => id === ref)
}

function findTrainingsFor ({ id }) {
  return this.ctx.trainings.filter(({ user_id, other_user_id }) => user_id === id || other_user_id === id)
}

function toDate (dateOrIcalTime) {
  const date = typeof dateOrIcalTime === 'string' ? new Date(dateOrIcalTime) : ('toJSDate' in dateOrIcalTime ? dateOrIcalTime.toJSDate() : dateOrIcalTime)
  return date.toLocaleString('fr-FR', { dateStyle: 'long', timeStyle: undefined })
}

function toDateTime (dateOrIcalTime) {
  const date = typeof dateOrIcalTime === 'string' ? new Date(dateOrIcalTime) : ('toJSDate' in dateOrIcalTime ? dateOrIcalTime.toJSDate() : dateOrIcalTime)
  return date.toLocaleString('fr-FR', { dateStyle: 'long', timeStyle: 'short' })
}

function durationToTime (isoDuration) {
  const hours = toHours('toICALString' in isoDuration ? isoDuration.toICALString() : isoDuration)

  return hours === 24 ? 'toute la journée' : `${hours.toLocaleString('fr-FR')}h`
}

function locationMode (input) {
  let mode = 'ADDRESS'
  let link = ''
  let location = (input || '').trim()

  if (location === '') {
    mode = 'EMPTY'
  }
  if (location.match(/zoom.us/) || location.match(/visio/i)) {
    mode = 'VISIO'
    link = location.match(/^http/) ? location : ''
  }
  else if (location.match(/^http/)) {
    mode = 'ROUTE'
    link = location
  }

  return { mode, location, link }
}

function inHours (minutes) {
  return Math.round(minutes / 60)
}


function inDays (minutes) {
  return Math.round(minutes / 60 / 6)
}

function location (input, remoteLabel, routeLabel) {
  const { mode, location, link } = locationMode(input)

  if (mode === 'EMPTY') {
    return ''
  }
  else if (mode === 'VISIO') {
    return `<i class="fas fa-video" aria-hidden="true"></i> ${remoteLabel}`
  }
  else if (mode === 'ROUTE') {
    return `<i class="fas fa-map-marker" aria-hidden="true"></i> <a href="${link}" target="_blank" rel="noreferrer noopener">${routeLabel}</a>`
  }
  else {
    return `<i class="fas fa-map-marker" aria-hidden="true"></i> <a href="https://www.google.com/maps?daddr=${location}&amp;hl=fr"  target="_blank" rel="noreferrer noopener">${location}</a>`
  }
}

function locationShort (input, remoteLabel, routeLabel) {
  const { mode, location } = locationMode(input)

  if (mode === 'VISIO') {
    return remoteLabel
  }
  else if (mode === 'ROUTE') {
    return routeLabel
  }
  else {
    return location.split(',').at(-1).trim()
  }
}



function pickattr (array, ...attributeNames) {
  return structuredClone(array).map(item => attributeNames.reduce((obj, attributeName) => {
    return { ...obj, [attributeName]: item[attributeName]}
  }, {}))
}

function removeattr (array, attributeName) {
  return structuredClone(array).map(item => {
    delete item[attributeName]
    return item
  })
}

function striptag (text, tag) {
  const re = new RegExp(`<${tag}[^>]*>([\s\S]+)<\/${tag}>`, 'gis')
  console.log(re.test(text), tag, `<${tag}[^>]*>([\s\S]+)<\/${tag}>`, text)
  return text.replace(re, '$1')
}

function where (array, attribute, expectation) {
  return array.filter(item => item[attribute] === expectation)
}

const encoders = {
  TYPES: ['ascii', 'utf8', 'random'],
  DEFAULT: 'random',

  ascii: {
    alreadyMunged: function(str) {
      if (str.startsWith('&#')) return str
    },

    munge: function(char) {
      return char && char.charCodeAt()
    }
  },

  utf8: {
    alreadyMunged: function(str) {
      if (str.startsWith('&#x')) return str
    },

    munge: function(char) {
      // toString(16) converts decimal (ascii) to hex
      var unicode = encoders.ascii.munge(char).toString(16).toUpperCase()

      // pad with leading zeros to ensure 4 bytes
      while (unicode.length < 4) {
        unicode = '0' + unicode
      }

      return 'x' + unicode
    }
  },

  random: {
    alreadyMunged: function (str) {
      return encoders.ascii.alreadyMunged(str) || encoders.utf8.alreadyMunged(str)
    },

    munge: function (char) {
      const type = encoders.TYPES[Math.floor(Math.random() * encoders.TYPES.length)]
      return encoders[type].munge(char)
    }
  }
}

/**
 * the one and only public function of this module. It takes any string and munges
 * it according to the options. By default it uses a random encoding.
 *
 *
 * @param {String} str any string to munge, for example 'spacemonkey@moon.com'
 * @param {Object} options for munging
 * @param options.encoding can be 'ascii', 'utf8' or 'random' (default)
 * @return {String} the munged string
 * @api public
 */
function munge (str, options) {
  const aMunged = []
  var aChars, i

  //  initialize default options
  options = options || {}

  if (options.encoding) {
    // validate encoding option
    if (encoders.TYPES.indexOf(options.encoding) < 0) {
      throw Error('Invalid encoding option given: ' + options.encoding)
    }
  } else {
    options.encoding = encoders.DEFAULT
  }

  const encoder = encoders[options.encoding]

  if (str) {
    if (encoder.alreadyMunged(str)) {
      return str
    }

    aChars = str.split('')

    for (i in aChars) {
      aMunged[i] = '&#' + encoder.munge(aChars[i]) + ';'
    }
  }

  return aMunged.join('')
}

module.exports.totalTime = totalTime
