const { join, normalize } = require('path')
const path = require('path')
const markdownItFootnote = require('markdown-it-footnote')
const markdownItAttrs = require('markdown-it-attrs')
const markdownItContainer = require('markdown-it-container')
const markdownItEleventyImage = require('markdown-it-eleventy-img')
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation")
const EleventyVitePlugin = require("@11ty/eleventy-plugin-vite")
const PurgeCssPlugin = require("eleventy-plugin-purgecss")
const SolsticePlugin = require('./js/filters.js')
const Image = require("@11ty/eleventy-img")
const { totalTime } = require('./js/filters.js')

const { ELEVENTY_RUN_MODE } = process.env
const { NEXTCLOUD_BASE_URL } = process.env
const { NEXTCLOUD_APPLICATION_USER:user } = process.env
const { NEXTCLOUD_APPLICATION_PASSWORD:password } = process.env
const { FORMASOL_API_ACCESS_TOKEN } = process.env

module.exports = function (eleventyConfig) {
  // Front-end setup
  eleventyConfig.addPlugin(EleventyVitePlugin)
  eleventyConfig.addPlugin(require("eleventy-plugin-dart-sass"), {
    sassLocation: normalize(join(__dirname, 'css/')),
    sassIndexFile: 'index.scss',
    perTemplateFiles: "vendor",
    outDir: normalize(join(__dirname, '_site')),
    outPath: '/css',
    includePaths: ['!node_modules/**', 'node_modules/bootstrap', 'node_modules/@fortawesome'],
    cacheBreak: Date.now(),
  })
  if (ELEVENTY_RUN_MODE == "build") {
    eleventyConfig.addPlugin(PurgeCssPlugin, {
      config: "./purgecess.config.js",
      quiet: false // Optional: Set quiet: true to suppress terminal output
    })
  }
  // Files that need to be copied to output dir as-is
  eleventyConfig.addPassthroughCopy("img")
  eleventyConfig.addPassthroughCopy("files")
  eleventyConfig.addPassthroughCopy("public")
  eleventyConfig.addPassthroughCopy("js/controllers/*")
  eleventyConfig.addPassthroughCopy("js/bootstrap.min.js")
  eleventyConfig.addPassthroughCopy({"node_modules/@fortawesome/fontawesome-free/webfonts": "css/fonts"})
  eleventyConfig.addPassthroughCopy("css/fonts/*")

  // Global defaults
  eleventyConfig.addGlobalData("layout", "contenu.njk")
  eleventyConfig.addGlobalData("temps", async () => totalTime('./le-temps-qui-passe.csv'))

  // Setup navigation
  eleventyConfig.addPlugin(eleventyNavigationPlugin)
  eleventyConfig.addGlobalData("navOptions", {
    includeSelf: true,
  })

  // Setup MarkDown conversion
  eleventyConfig.amendLibrary("md", mdLib => {
    mdLib.disable('code')
    mdLib.use(markdownItFootnote)
    mdLib.use(markdownItEleventyImage, {
      imgOptions: {
        widths: [480, 860],
        urlPath: "/img/",
        outputDir: "./_site/img/",
        formats: ["auto"]
      },
      globalAttributes: {
        class: "markdown-image",
        decoding: "async",
        loading: "lazy",
        sizes: "(max-width: 480px) 100vw, 100vw"
      },
      renderImage(image, attributes) {
        const [ Image, options ] = image
        const [ src, attrs ] = attributes

        Image(src, options)
        const metadata = Image.statsSync(src, options)
        return Image.generateHTML(metadata, attrs, {
          whitespaceMode: "inline"
        })
      }
    })
    mdLib.use(markdownItAttrs, { allowedAttributes: ['id', 'class', 'target']})
    mdLib.use(markdownItContainer, 'info', {
      render (tokens, idx) {
        if (tokens[idx].nesting === 1) {
          return `<div class="alert alert-info" role="alert">
          <h2 class="alert-heading">${tokens[idx].info.trim().replace(/^\s*info\s*/, '')}</h2>
          <div class="alert-body">
          `
        }
        else {
          return `</div></div>`
        }
      }
    })
  })

  // Add image shortcode. Usage: {% image src, alt [, attributes] %}
  eleventyConfig.addAsyncShortcode("image", async function(src, alt, attributes = {}) {
    let metadata = await Image(src, {
      formats: ["auto"],
      widths: [480, 860],
      outputDir: "./_site/img/",
      cacheOptions: {
        duration: "1w"
      }
    })
    const sizes = "(max-width: 480px) 100vw, 100vw"

    const imageAttributes = {
      alt,
      sizes,
      ...attributes,
      loading: "lazy",
      decoding: "async",
    }
    return Image.generateHTML(metadata, imageAttributes);
  })

  // Setup site-specific globals
  eleventyConfig.addGlobalData("nextcloudUrl", NEXTCLOUD_BASE_URL)
  eleventyConfig.addGlobalData("formasolToken", FORMASOL_API_ACCESS_TOKEN)
  eleventyConfig.addPlugin(SolsticePlugin)

  // Setup "temps forts" as a collection
  eleventyConfig.addCollection("posts", collectionApi => {
    return collectionApi
      .getFilteredByGlob('pages/cooperative/temps-forts/*.md')
      .filter(({ inputPath }) => inputPath.endsWith('index.md') === false)
      .sort((a, b) => b.date - a.date)
  })

  return {
    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dir: {
      input: "pages",
      includes: "../_layouts",
      layouts: "../_layouts",
      data: "../_data",
    }
  }
}
