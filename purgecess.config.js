module.exports = {
  // Content files referencing CSS classes
  // Include JS files to preserve dynamic classes, if necessary
  content: [
    "./_site/**/*.html",
    "./_site/**/*.js"
  ],

  // CSS files to be purged (in-place, since we're working on output files)
  css: ["./_site/**/*.css"],
}
