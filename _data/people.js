const EleventyFetch = require("@11ty/eleventy-fetch")
const { FORMASOL_API_ACCESS_TOKEN } = process.env

module.exports = async function() {
  // limit is 100 items for now
  // @see https://airtable.com/appywAxghnSzkqNEp/api/docs#curl/table:liste:list
  const people = await EleventyFetch(`https://formasol.solstice.coop/api/users.json`, {
    duration: '3h',
    type: 'json',
    fetchOptions: {
      headers: {
        "Authorization": `Token token=\"${FORMASOL_API_ACCESS_TOKEN}\"`
      }
    }
  })

  return people.filter(({ first_name }) => !/admin/i.test(first_name))
};
